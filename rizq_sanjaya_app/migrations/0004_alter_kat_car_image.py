# Generated by Django 3.2 on 2023-07-23 17:16

from django.db import migrations, models
import rizq_sanjaya_app.models.profile


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0003_kat_car'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kat_car',
            name='image',
            field=models.ImageField(upload_to='profile/images/kategori/', validators=[rizq_sanjaya_app.models.profile.validate_file_gambar, rizq_sanjaya_app.models.profile.validate_file_size_gambar]),
        ),
    ]
