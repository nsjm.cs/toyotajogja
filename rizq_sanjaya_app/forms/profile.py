from django import forms
from ..models import *
from PIL import Image
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from captcha.fields import CaptchaField

class CategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = ('id', 'nama','typexs',)

class NewsForm(forms.ModelForm):
	class Meta:
		model = News
		fields = ('title', 'content', 'category', 'tags',)

class RnWForm(forms.ModelForm):
	class Meta:
		model = RnW
		fields = ('jenis', 'deskripsi',)

class NewsThumbnailForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = News
		fields = ('thumbnail', 'x', 'y', 'width', 'height',)

	def save(self):
		news = super(NewsThumbnailForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(news.thumbnail)
		image.save(f"{settings.MEDIA_ROOT}{news.thumbnail.name}")

		## SAVE THUMBNAIL
		# image = Image.open(news.thumbnail)
		# cropped_image = image.crop((x, y, w + x, h + y))
		# cropped_image.save(f"{settings.MEDIA_ROOT}{news.thumbnail.name}")
		lokasi = f"{news.thumbnail.name}"
		hassss = lokasi.replace('artikel/', 'artikel/thumb/')

		tumbnil = image.crop((x, y, w + x, h + y))
		tumbnis = tumbnil.resize((w, h), Image.ANTIALIAS)
		tumbnis.save(f"{settings.MEDIA_ROOT}"+hassss)

		return news
class NewsFormTanpaFile(forms.ModelForm):
	class Meta:
		model = News
		fields = ('title', 'content', 'category', 'tags',)

class GalleryForm(forms.ModelForm):
	class Meta:
		model = Gallery
		fields = ('title', 'keterangan', 'video', 'tags',)

class GalleryFotoForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = Gallery
		fields = ('foto',)

	def save(self):
		gallery = super(GalleryFotoForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(gallery.foto)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{gallery.foto.name}")

		return gallery

class AvatarForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = Account
		fields = ('avatar', 'x', 'y', 'width', 'height',)

	def save(self):
		account = super(AvatarForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(account.avatar)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{account.avatar.name}")

		return account

class AccountForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('first_name', 'last_name', 'phone', 'date_of_birth')

class RoleForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('role',)

class CustomUserCreationForm(UserCreationForm):
	class Meta:
		model = Account
		fields = ('email', 'username', 'first_name', 'last_name', 'role', 'phone', 'date_of_birth',)

class RunningTextForm(forms.ModelForm):
	class Meta:
		model = RunningText
		fields = ('text',)

class LinkForm(forms.ModelForm):
	class Meta:
		model = Link
		fields = ('nama', 'link')

class AppSettingForm(forms.ModelForm):
	class Meta:
		model = AppSetting
		fields = ('nama', 'keterangan')

class CarouselForm(forms.ModelForm):
	class Meta:
		model = Carousel
		fields = ('judul', 'subjudul', 'keterangan', 'link')

class CarouselFotoForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = Carousel
		fields = ('foto',)

	def save(self):
		carousel = super(CarouselFotoForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(carousel.foto)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{carousel.foto.name}")

		return carousel

class RunningLinkForm(forms.ModelForm):
	class Meta:
		model = RunningLink
		fields = ('nama', 'link')

class RunningLinkFotoForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = RunningLink
		fields = ('logo',)

	def save(self):
		running_link = super(RunningLinkFotoForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(running_link.logo)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{running_link.logo.name}")

		return running_link

class MyCaptcha(forms.Form):
   captcha=CaptchaField()

class AboutMeForm(forms.ModelForm):
	class Meta:
		model = AboutMe
		fields = ('judul', 'deskripsi', 'youtube', 'jenis', 'section',) 

class LayananForm(forms.ModelForm):
	class Meta:
		model = Layanan
		fields = ('judul', 'keterangan', 'icon', 'slug',)

class ProductForm(forms.ModelForm):
	class Meta:
		model = Product
		fields = ('product_name', 'category', 'best_seller', 'description', 'tags', 'harga')

class ProductDiskonForm(forms.ModelForm):
    diskon = forms.CharField(required=False)
    persen_diskon = forms.CharField(required=False)

    class Meta:
        model = Product
        fields = ('diskon', 'persen_diskon')


class ProductImagesForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	
	class Meta:
		model = Product
		fields = ('images', 'x', 'y', 'width', 'height',)

	def save(self):
		product = super(ProductImagesForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(product.images)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{product.images.name}")

		return product