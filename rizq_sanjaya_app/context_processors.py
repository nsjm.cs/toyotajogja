import os
import numpy

from datetime import datetime
from .models.profile import AppSetting
from .helpers import get_list_berita

def global_variables(request):
    # links = Link.objects.all()
    # menu_pages = Halaman.objects.filter(deleted_at=None).order_by('id')
    # notif_complaint = Complaint.objects.filter(deleted_at=None,  read_status=False).count()
    # notif_guestbook = GuestBook.objects.filter(deleted_at=None,  read_status=False).count()
    # payment_footer = AboutMe.objects.filter(jenis='header', section='payment')

    dt_news = get_list_berita(2)
    app_setting = AppSetting.objects.all()
    nama_website = alamat = alamat_link = embed_maps = telepon = telepon_text  = whatsapp = whatsapp_text = email = 'Belum Disetting'
    meta_keywords = meta_author = meta_property_title = meta_property_description = 'Belum Disetting'
    link_facebook = link_twitter = link_youtube = link_instagram = link_tiktok = 'Belum Disetting'
    header_layanan = header_galeri = header_kontak = 'Belum Disetting' 
    about_text = newletter_text = 'Lorem ipsum dolor sit amet'
    dt_kategori_mobil = kat_car.objects.filter(deleted_at=None).order_by('-id')

    for appx in app_setting:
        if appx.nama == 'nama_website':
            nama_website = appx.keterangan
        elif appx.nama == 'alamat':
            alamat = appx.keterangan
        elif appx.nama == 'alamat_link':
            alamat_link = appx.keterangan
        elif appx.nama == 'embed_maps':
            embed_maps = appx.keterangan
        elif appx.nama == 'telepon':
            telepon = appx.keterangan
        elif appx.nama == 'telepon_text':
            telepon_text = appx.keterangan
        elif appx.nama == 'whatsapp':
            whatsapp = appx.keterangan
        elif appx.nama == 'whatsapp_text':
            whatsapp_text = appx.keterangan
        elif appx.nama == 'email':
            email = appx.keterangan
        elif appx.nama == 'meta_keywords':
            meta_keywords = appx.keterangan
        elif appx.nama == 'meta_author':
            meta_author = appx.keterangan
        elif appx.nama == 'meta_property_title':
            meta_property_title = appx.keterangan
        elif appx.nama == 'meta_property_description':
            meta_property_description = appx.keterangan
        elif appx.nama == 'link_facebook':
            link_facebook = appx.keterangan
        elif appx.nama == 'link_instagram':
            link_instagram = appx.keterangan
        elif appx.nama == 'link_youtube':
            link_youtube = appx.keterangan
        elif appx.nama == 'link_twitter':
            link_twitter = appx.keterangan
        elif appx.nama == 'link_tiktok':
            link_tiktok = appx.keterangan
        elif appx.nama == 'header_layanan':
            header_layanan = appx.keterangan
        elif appx.nama == 'header_galeri':
            header_galeri = appx.keterangan
        elif appx.nama == 'header_kontak':
            header_kontak = appx.keterangan
        elif appx.nama == 'about_text': 
            about_text = appx.keterangan
        elif appx.nama == 'newletter_text':
            newletter_text = appx.keterangan

    domain = '127.0.0.1:8000'
    try:
        domain = request.get_host()
    except:
        pass

    # deskripsi_pay = 'Cara pembayaran belum disetting, silahkan hubungi administrator.'
    # image_1 = image_2 = ''
    # for x in payment_footer:
    #     deskripsi_pay = x.deskripsi if x.deskripsi != None else 'Cara pembayaran belum disetting, silahkan hubungi administrator.'
    #     image_1 = x.youtube
    #     image_2 = x.icon
            
    return {
        'nama_website': nama_website,
        'alamat': alamat,
        'alamat_link': alamat_link,
        'embed_maps': embed_maps,
        'telepon': telepon,
        'telepon_text': telepon_text,
        'whatsapp': whatsapp,
        'whatsapp_text': whatsapp_text,
        'email': email,
        'meta_keywords': meta_keywords,
        'meta_author': meta_author,
        'meta_property_title': meta_property_title,
        'meta_property_description': meta_property_description,
        'link_facebook': link_facebook,
        'link_youtube': link_youtube,
        'link_twitter': link_twitter,
        'link_instagram': link_instagram,
        'link_tiktok': link_tiktok,

        'about_text' : about_text,
        'newletter_text' : newletter_text,
        # 'links': links,
        # 'menu_pages': menu_pages,
        # 'notif_complaint': notif_complaint,
        # 'notif_guestbook': notif_guestbook,
        'domain': domain,
        'header_layanan': header_layanan,
        'header_galeri': header_galeri,
        'header_kontak': header_kontak,
        'dt_news': dt_news,
        'data_kat_mobil': dt_kategori_mobil,
        # 'payment_footer': {'deskripsi':deskripsi_pay, 'image_1': image_1, 'image_2': image_2},
    }
