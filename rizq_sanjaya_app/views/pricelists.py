from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category, get_category_mobil

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    PricelistMobil = Pricelist.objects.all().order_by('-created_at', )
    archives = Pricelist.objects.filter(deleted_at__isnull=False).order_by('-created_at')
    paginator = Paginator(PricelistMobil, 15)
    try:
        PricelistMobil = paginator.page(page)
    except PageNotAnInteger:
        PricelistMobil = paginator.page(1)
    except EmptyPage:
        PricelistMobil = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Mobil - Admin',
        'pricelist'   : PricelistMobil,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/pricelist/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = Mobil.objects.all()
        context = {
            'title' : 'ADD Mobil',
            'form' : form,
        }

        template = 'profile/admin/pricelist/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        kategori = request.POST.get('kategori')
        nama = request.POST.get('nama')
        manual = request.POST.get('manual')
        matic = request.POST.get('matic')
        
        try:
            
            dt_mobil = Pricelist()
            dt_mobil.typex = nama
            dt_mobil.manual = manual
            dt_mobil.matic = matic
           
            dt_kategori = Mobil.objects.get(id=kategori)
            dt_mobil.id_mobil = dt_kategori
            dt_mobil.save()
                
            messages.success(request, 'Data berhasil disimpan.')
            return redirect('profile:admin_pricelist_mobil')
        except Exception as error:
            print(error)
            messages.error(request, f'Terjadi Kesalahan saat menyimpan data karena {str(error)}.')
            return redirect('profile:admin_pricelist_mobil')
    
@login_required
@is_verified()
def edit(request, id):
    if request.method == 'GET':
        news = Pricelist.objects.get(id = id)
        form = Mobil.objects.all()

        context = {
            'title' : 'EDIT Kategori Mobil',
            'form' : form,
            'edit' : 'true',
            'news' : news,
        }
        template = 'profile/admin/pricelist/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        kategori = request.POST.get('kategori')
        nama = request.POST.get('nama')
        manual = request.POST.get('manual')
        matic = request.POST.get('matic')
        
        try:
            
            dt_mobil = Pricelist.objects.get(id = id)
            dt_mobil.typex = nama
            dt_mobil.manual = manual
            dt_mobil.matic = matic
           
            dt_kategori = Mobil.objects.get(id=kategori)
            dt_mobil.id_mobil = dt_kategori
            dt_mobil.save()
                
            messages.success(request, 'Data berhasil disimpan.')
            return redirect('profile:admin_pricelist_mobil')
        except Exception as error:
            print(error)
            messages.error(request, f'Terjadi Kesalahan saat menyimpan data karena {str(error)}.')
            return redirect('profile:admin_pricelist_mobil')

@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = Pricelist.objects.get(id=id)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except Pricelist.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return redirect('profile:admin_pricelist_mobil')

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = Pricelist.objects.get(id=id)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except Pricelist.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return redirect('profile:admin_pricelist_mobil')

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = kat_car.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except kat_car.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
