from .admin import *
from .dashboard import *
from .gallery import *
from .news import *
from .tags import *
from .user import *
from .setting import *
from .layanan import *
from .services import *

from .halaman import *
from .product import *
from .kata import *
from .keunggulan import *
from .kategorimobil import *
from .mobil import *
from .mobil_front import *
from .pricelists import *