from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import read_boxicons_css


@login_required
@is_verified()
@require_http_methods(["GET"])
def child_index(request, id):
    data = Child.objects.filter(mobil = id).order_by('-created_at')
    x = {
        'title': 'Child Mobil',
        'id' : id,
        'data' : data,
    }
    return render(request, 'profile/admin/mobil/child/index.html', x)

@login_required
@is_verified()
def child_create(request, id):
    fonticon = read_boxicons_css()
    limit = 3
    total = len(fonticon)
    hasil = []

    for i in range(0, total, limit):
        hasil.append(fonticon[i:i + limit])

    try:
        user = Mobil.objects.get(id=id)
    except Mobil.DoesNotExist:
        messages.error(request, 'Orang tua tidak ditemukan.')
        return redirect('profile:admin_child')

    if request.method == 'GET':
        x = {
            'title': 'ADD Child',
            'dticon': hasil,
            'id': id,
        }
        return render(request, 'profile/admin/mobil/child/create.html', x)

    if request.method == 'POST':
        judul = request.POST.get('judul')
        subjudul = request.POST.get('deskripsi')
        order = request.POST.get('order')
        icon = request.POST.get('icon')

        try:
            AboutMe_add = Child()
            AboutMe_add.nama = judul
            AboutMe_add.deskripsi = subjudul
            AboutMe_add.icon = icon
            AboutMe_add.mobil = user
            AboutMe_add.save()

            messages.success(request, 'Data berhasil disimpan.')
            return redirect('profile:admin_child', id=id)
        except Exception as e:
            messages.error(request, f'Terjadi Kesalahan Saat Menyimpan Data: {str(e)}')

    x = {
        'title': 'ADD Child',
        'dticon': hasil,
        'id': id,
    }

    return render(request, 'profile/admin/mobil/child/create.html', x)

@login_required
@is_verified()
def child_edit(request, id, child):
    fonticon = read_boxicons_css()
    limit = 3
    total = len(fonticon)
    hasil = []

    for i in range(0,total,limit):
        hasil.append(fonticon[i:i+limit])

    if request.method == 'GET':
        data = Child.objects.get(child_id = child)
        x = {
            'title': 'EDIT Child',
            'dticon' : hasil,
            'id' : id,
            'edit' : 'true',
            'data' : data,
        }
        return render(request, 'profile/admin/mobil/child/create.html', x)
    
    if request.method == 'POST':
        judul = request.POST.get('judul')
        subjudul = request.POST.get('deskripsi')
        order = request.POST.get('order')
        icon = request.POST.get('icon')
        print(order)

        try:
            AboutMe_add = Child.objects.get(child_id = child)
            AboutMe_add.nama = judul
            AboutMe_add.deskripsi = subjudul
            AboutMe_add.icon = icon
            AboutMe_add.save()

            messages.success(request, 'Data berhasil disimpan.')
            return redirect(reverse('profile:admin_child', args=[id]))
        except Exception as f:
            messages.error(request, f'Terjadi Kesalahan Saat Menyimpan Data {str(f)}.')
            return redirect(reverse('profile:admin_child', args=[id]))
        
@login_required
@is_verified()
def permanentDeletechild(request, id, child):
    message = ''
    try:
        doc = Child.objects.get(child_id=child)
        doc.delete()
        message = 'success'
    except Child.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return redirect(reverse('profile:admin_child', args=[id]))
        
@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    
    mobil = Mobil.objects.filter(deleted_at__isnull=True).order_by('-created_at', ).prefetch_related('mobilimage_set').all()
    news = Mobil.objects.filter(deleted_at__isnull=True).order_by('-created_at', )
    archives = Mobil.objects.filter(deleted_at__isnull=False).order_by('-created_at')
    paginator = Paginator(news, 15)
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Mobil - Admin',
        'mobil' : news,
        'data_mobil'   : mobil,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/mobil/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = kat_car.objects.all()
        context = {
            'title' : 'ADD Mobil',
            'form' : form,
        }

        template = 'profile/admin/mobil/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        image = request.FILES.getlist('images')
        kategori = request.POST.get('kategori')
        nama = request.POST.get('nama')
        harga = request.POST.get('harga')
        deskripsi = request.POST.get('deskripsi')
        best = request.POST.get('best')
        print(best)
        
        try:
            
            dt_mobil = Mobil()
            dt_mobil.nama = nama
            dt_mobil.harga = harga
            dt_mobil.deskripsi = deskripsi
            if best == 'yes':
                dt_mobil.best_seller = True
            if best == 'no':
                dt_mobil.best_seller = False
            
           
            dt_kategori = kat_car.objects.get(id=kategori)
            dt_mobil.kategori_mobil = dt_kategori
            dt_mobil.save()
            faqs = []
            for key in request.POST:
                if key.startswith('fasilitas') and key[9:].isdigit():
                    print('hahaha2')
                    index = int(key[9:])
                    print(index)
                    deskripsi = request.POST[key]
                    icon = request.POST.get(f'icon{index}', '')
                    judul = request.POST.get(f'judul{index}', '')
                    if icon and judul:
                        mbl = get_object_or_404(Mobil, id = dt_mobil.id)
                        faq = childmobil(nama=judul, deskripsi=deskripsi, icon=icon, idchildmobil=mbl)
                        faqs.append(faq)
                    else:
                        print(f"Missing data for index {index}")
                    
            # Simpan semua FAQ baru ke database
            childmobil.objects.bulk_create(faqs)
            for x in image:
                mobil_id = get_object_or_404(Mobil, id = dt_mobil.id)
                
                img = MobilImage()
                img.mobil = mobil_id
                img.images = x
                img.save()
                
            messages.success(request, 'Data  Mobil berhasil disimpan.')
            return redirect('profile:admin_mobil')
        except Exception as error:
            print(error)
            messages.error(request, 'Kategori Mobil gagal disimpan.')
            return render(request, 'profile/admin/mobil/create.html', {'form': form,})
    
@login_required
@is_verified()
def edit(request, id):
    template = 'profile/admin/mobil/create.html'
    form = kat_car.objects.all()
    mobil = get_object_or_404(Mobil, id=id)

    if request.method == 'GET':
        context = {
            'title': 'EDIT Mobil',
            'form': form,
            'edit': 'true',
            'mobil': mobil,
        }
        return render(request, template, context)

    if request.method == 'POST':
        try:
            # Filter MobilImage objects for the given Mobil object
            datagambar = MobilImage.objects.filter(mobil__id=id)

            # Handle other form fields (kategori, nama, harga, deskripsi, etc.)
            kategori = request.POST.get('kategori')
            nama = request.POST.get('nama')
            harga = request.POST.get('harga')
            deskripsi = request.POST.get('deskripsi')
            harga_clean = harga.replace('.', '')
            best = request.POST.get('best')
            
            dt_mobil = get_object_or_404(Mobil, id=id)
            dt_mobil.nama = nama
            dt_mobil.harga = int(harga_clean)
            dt_mobil.deskripsi = deskripsi
            if best == 'yes':
                dt_mobil.best_seller = True
            if best == 'no':
                dt_mobil.best_seller = False
            dt_kategori = get_object_or_404(kat_car, id=kategori)
            dt_mobil.kategori_mobil = dt_kategori
            dt_mobil.save()

            # Save new MobilImage objects
            if 'images' in request.FILES:
                # Remove old image files and delete MobilImage records
                for mobil_image in datagambar:
                    images_field_value = mobil_image.images
                    path_file_lama = os.path.join(settings.MEDIA_ROOT, str(images_field_value))
                    try:
                        # Remove old image files
                        os.remove(path_file_lama)
                    except Exception as e:
                        print(f"Error deleting old image: {e}")

                # Delete MobilImage records
                datagambar.delete()
                images = request.FILES.getlist('images')
                for img in images:
                    img_obj = MobilImage(mobil=dt_mobil, images=img)
                    img_obj.save()

            faqs = []
            for key in request.POST:
                if key.startswith('fasilitas') and key[9:].isdigit():
                    index = int(key[9:])
                    deskripsi = request.POST[key]
                    icon = request.POST.get(f'icon{index}', '')
                    judul = request.POST.get(f'judul{index}', '')
                    if icon and judul:
                        faq = childmobil(nama=judul, deskripsi=deskripsi, icon=icon, idchildmobil=dt_mobil)
                        faqs.append(faq)
                    else:
                        print(f"Missing data for index {index}")
                        
            # Simpan semua FAQ baru ke database
            childmobil.objects.bulk_create(faqs)

            messages.success(request, 'Data Mobil berhasil disimpan.')
            return redirect('profile:admin_mobil')
        except Exception as error:
            print(error)
            messages.error(request, 'Kategori Mobil gagal disimpan.')
            return render(request, template, {'form': form})


@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, id):
    template = 'profile/admin/kategori/detail.html'
    try:
        news = kat_car.objects.get(id=id)
    except kat_car.DoesNotExist:
        news = None
    context = {
        'title' : news.judul if news != None else 'TIDAK DITEMUKAN',
        'news' : news,
    }
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = Mobil.objects.get(id=id)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except Mobil.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = Mobil.objects.get(id=id)
        try:
            image_car = MobilImage.objects.filter(mobil__id=id)
            image_car.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except Mobil.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = Mobil.objects.get(id=id)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except Mobil.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)