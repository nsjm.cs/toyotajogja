from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import GuestBook
import os
from django.conf import settings
from django.utils import timezone
from ..decorators import *

from captcha.models import CaptchaStore
import json

@require_http_methods(["GET", "POST"])
def index(request):
    template = ''
    context = {}
    form = ''

    if request.method == 'GET':
        runlink = RunningLink.objects.filter(deleted_at=None).order_by('id')
        for x in runlink:
            x.images = os.path.isfile(settings.MEDIA_ROOT+str(x.logo))

        form = GuestBookForm()
        guestbook = GuestBook.objects.filter(deleted_at=None, status='t').order_by('-created_at')

        context = {
            'title' : 'Buku Tamu',
            'form' : form,
            'guestbooks': guestbook,
            'runlink' : runlink,
            'captcha' : MyCaptcha(),
        }

        template = 'profile/guestbook/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = GuestBookForm(data=request.POST)
        captcha = MyCaptcha(request.POST)

        if captcha.is_valid():
            if form.is_valid():
                form.save()
                messages.success(request, 'Pesan berhasil disimpan.')
                return redirect('profile:guestbook')
            else:
                messages.error(request, 'Pesan gagal disimpan.')
        else:
            messages.error(request, 'Pesan gagal disimpan. Captcha tidak sama.')

        return redirect('profile:guestbook')

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    guestbook = GuestBook.objects.filter(deleted_at__isnull=True).order_by('-created_at')
    archives = GuestBook.objects.filter(deleted_at__isnull=False).order_by('-created_at')
    context = {
        'title' : 'Buku Tamu - Admin',
        'guestbooks' : guestbook,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/guestbook/index.html', context)

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, slug):
    template = 'profile/admin/guestbook/detail.html'
    try:
        guestbook = GuestBook.objects.get(slug=slug)
        guestbook.read_status=True
        guestbook.save()
    except GuestBook.DoesNotExist:
        guestbook = None
    context = {
        'title' : 'Buku Tamu' if guestbook != None else 'TIDAK DITEMUKAN',
        'guestbook' : guestbook,
    }
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, slug):
    message = ''
    try:
        sekarang = timezone.now()
        guestbook = GuestBook.objects.get(slug=slug)
        guestbook.deleted_at = sekarang
        guestbook.save()
        message = 'success'
    except GuestBook.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, slug):
    message = ''
    try:
        guestbook = GuestBook.objects.get(slug=slug)
        guestbook.delete()
        message = 'success'
    except GuestBook.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, slug):
    message = ''
    try:
        guestbook = GuestBook.objects.get(slug=slug)
        guestbook.deleted_at = None
        guestbook.save()
        message = 'success'
    except GuestBook.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@require_http_methods(["GET"])
def captcha_refresh(request):

    if not request.is_ajax():
        raise Http404

    new_key = CaptchaStore.generate_key()
    to_json_response = {
        'key' : new_key,
        'image_url' : '/captcha/image/'+new_key,
    }

    return HttpResponse(json.dumps(to_json_response), content_type=' application/json ')

@login_required
@is_verified()
def tampilkan(request, slug):
    message = ''
    try:
        guestbook = GuestBook.objects.get(slug=slug)
        guestbook.status = True
        guestbook.deleted_at = None
        guestbook.save()
        message = 'success'
    except GuestBook.DoesNotExist:
        message = 'error'
    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def sembunyikan(request, slug):
    message = ''
    try:
        # sekarang = timezone.now()
        guestbook = GuestBook.objects.get(slug=slug)
        guestbook.status = False
        # guestbook.deleted_at = sekarang
        guestbook.save()
        message = 'success'
    except GuestBook.DoesNotExist:
        message = 'error'
    context = {
            'message' : message,
        }

    return HttpResponse(context)