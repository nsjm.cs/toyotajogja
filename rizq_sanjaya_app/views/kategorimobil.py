from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import News,Category
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kategori_mobil = kat_car.objects.filter(deleted_at__isnull=True).order_by('-created_at', )
    news = kat_car.objects.filter(deleted_at__isnull=True).order_by('-created_at', )
    archives = kat_car.objects.filter(deleted_at__isnull=False).order_by('-created_at')
    paginator = Paginator(news, 15)
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Keunggulan - Admin',
        'kategori_mobil' : news,
        'category_car'   : kategori_mobil,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/kategori/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = kat_car.objects.all()
        context = {
            'title' : 'ADD Kategori Mobil',
            'form' : form,
        }

        template = 'profile/admin/kategori/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        image = request.FILES['image']
        judul = request.POST.get('judul')
        # keterangan = request.POST.get('keterangan')
        if judul is not None:
            new_news = kat_car()
            new_news.image = image
            new_news.judul = judul
            # new_news.keterangan = keterangan
            new_news.save()
            
            messages.success(request, 'Kategori Mobil berhasil disimpan.')
            return redirect('profile:admin_kateogri_mobil')

        messages.error(request, 'Kategori Mobil gagal disimpan.')
        return render(request, 'profile/admin/kategori/create.html', {'form': form,})
    
@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        news = kat_car.objects.get(id = id)

        context = {
            'title' : 'EDIT Kategori Mobil',
            'form' : form,
            'edit' : 'true',
            'news' : news,
        }
        template = 'profile/admin/kategori/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        datakategori = kat_car.objects.get(id = id)
        path_file_lama = f"{settings.MEDIA_ROOT}/{datakategori.image}"
        foto_old = bool(datakategori.image)
        judul = request.POST.get('judul')
        

        if datakategori is not None:
            datakategori.judul = judul
            datakategori.save()
    
            if 'image' in request.FILES:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                foto = request.FILES['image']
                datakategori.image = foto
                datakategori.save()

            messages.success(request, 'Kategori Mobil berhasil disimpan')
            return redirect('profile:admin_kateogri_mobil')

        messages.error(request, 'Kategori gagal disimpan.')
        return render(request, 'profile/admin/kategori/create.html', {'form': form,})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request, id):
    template = 'profile/admin/kategori/detail.html'
    try:
        news = kat_car.objects.get(id=id)
    except kat_car.DoesNotExist:
        news = None
    context = {
        'title' : news.judul if news != None else 'TIDAK DITEMUKAN',
        'news' : news,
    }
    return render(request, template, context)

@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = kat_car.objects.get(id=id)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except kat_car.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = kat_car.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except kat_car.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = kat_car.objects.get(id=id)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except kat_car.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)