<!doctype html>
<html lang="en">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="icon" href="img/favicon-red.png" type="image/png">
      <title>Toyotajogja.id - Marketing Toyota Jogja</title>
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="vendors/icomoon-icon/style.css">
      <link rel="stylesheet" href="vendors/themify-icon/themify-icons.css">
      <link rel="stylesheet" href="vendors/datetimepicker/tempusdominus-bootstrap-4.min.css">
      <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
      <link rel="stylesheet" href="vendors/owl-carousel/assets/owl.carousel.min.css">
      <link rel="stylesheet" href="vendors/slick/slick-theme.css">
      <link rel="stylesheet" href="vendors/slick/slick.css">
      <link rel="stylesheet" href="vendors/animation/animate.css">
      <link rel="stylesheet" href="vendors/popup/magnific-popup.css">
      <link rel="stylesheet" href="vendors/animate-css/animate.css">
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="css/responsive.css">
   </head>
   <body data-scroll-animation="true">
      <div class="preloader">
         <div class="main-loader">
            <span class="loader1"></span>
            <span class="loader2"></span>
            <span class="loader3"></span>
         </div>
      </div>
     <!-- menu -->
    <?php //include 'header.php'; ?>
    <header class="header_area menu_two menu_ten">
         <div class="top_bus_menu">
            <div class="container">
               <div class="d-flex justify-content-between">
                    <div class="left">
                        <h6><a href=""><i class="icon-clock"></i>Senin-Sabtu 08:00 - 16:00 WIB</h6>
                    </div>
                    <div class="right">
                        <a href="#"><i class="fa fa-whatsapp fa-2x"></i><span>+62</span> 818-0211-0288<span></span></a>
                        <p><i class="icon-map_marker"></i> Kasihan, Bantul, DIY</p>
                    </div>
               </div>
            </div>
         </div>
         <div class="main_menu">
            <div class="container">
               <nav class="navbar navbar-expand-lg navbar-light bg-light">
                  <a class="navbar-brand" href="index.php"><img src="img/logo/logo-l-toyotajogja.png" srcset="img/truck-logo-2x.png 2x" alt="bar"></a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="active">
                           <a href="index.php">Beranda</a>
                        </li>
                        <li class="dropdown submenu">
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Mobil</a>
                           <ul class="dropdown-menu">
                              <li><a href="pricelist.php">Hatchback</a></li>
                              <li><a href="pricelist.php">MPV</a></li>
                              <li><a href="pricelist.php">SUV</a></li>
                              <li><a href="pricelist.php">Sedan</a></li>
                              <li><a href="pricelist.php">Sport</a></li>
                              <li><a href="pricelist.php">Commercial</a></li>
                           </ul>
                        </li>
                        <li><a href="pricelist.php">PRICELIST</a></li>
                        <li><a href="promo.php">PROMO</a></li>
                        <li><a href="info.php">Berita</a></li>
                        <li><a href="#" class="cart_bag_btn">Kontak</a></li>
                     </ul>
                     
                  </div>
               </nav>
            </div>
         </div>
      </header>
     <!-- end menu -->
      <section class="main_slider_area bus_slider_area">
         <div class="main_slider bus_slider classified_slider">
            <div class="slider_item">
               <div class="image_overlay" style="background: url(img/slider/fortuner-toyotajogjaid.jpg) no-repeat;"></div>
               <div class="container">
                  <div class="slider_text">
                     <!-- <h2 data-animation="fadeInUp" data-delay="0.5s">Find the <span>best car</span> <br/>for you</h2>
                     <p data-animation="fadeInUp" data-delay="800ms">With everyday low fares and hundreds of direct routes, <br/>everyone can explore new places!</p>
                     <a data-animation="fadeInDown" data-delay="1s" class="main_btn orange" href="#">View Collection</a>
                     <a data-animation="fadeInDown" data-delay="1s" class="main_btn orange" href="#">Get Quote</a> -->
                  </div>
               </div>
            </div>
            <div class="slider_item">
               <div class="image_overlay" style="background: url(img/slider/inova-toyotajogjaid.jpg) no-repeat;"></div>
               <div class="container">
                  <div class="slider_text">
                     <!-- <h2 data-animation="fadeInUp" data-delay="0.5s">Find the <span>best car</span> <br/>for you</h2>
                     <p data-animation="fadeInUp" data-delay="800ms">With everyday low fares and hundreds of direct routes, <br/>everyone can explore new places!</p>
                     <a data-animation="fadeInDown" data-delay="1s" class="main_btn orange" href="#">View Collection</a>
                     <a data-animation="fadeInDown" data-delay="1s" class="main_btn orange" href="#">Get Quote</a> -->
                  </div>
               </div>
            </div>
            <div class="slider_item">
               <div class="image_overlay" style="background: url(img/slider/avanza-toyotajogjaid.jpg) no-repeat;"></div>
               <div class="container">
                  <div class="slider_text">
                     <!-- <h2 data-animation="fadeInUp" data-delay="0.5s">Find the <span>best car</span> <br/>for you</h2>
                     <p data-animation="fadeInUp" data-delay="800ms">With everyday low fares and hundreds of direct routes, <br/>everyone can explore new places!</p>
                     <a data-animation="fadeInDown" data-delay="1s" class="main_btn orange" href="#">View Collection</a>
                     <a data-animation="fadeInDown" data-delay="1s" class="main_btn orange" href="#">Get Quote</a> -->
                  </div>
               </div>
            </div>
         </div>
         <div class="slider_nav slider_nav_four">
            <i class="arrow_left ti-angle-left"></i>
            <i class="arrow_right ti-angle-right"></i>
         </div>
      </section>
      <section class="find_form">
         <div class="container">
            <div class="find_title">
               <i class="icon-car_2"></i> Simulasi Kredit
            </div>
            <div class="find_form_inner row m0">
               <div class="find_item">
                  <select class="nice_select">
                     <option value="1">Pilih Mobil</option>
                     <option value="2">Compact</option>
                     <option value="3">Coupe</option>
                  </select>
               </div>
               <div class="find_item">
                  <select class="nice_select">
                     <option value="1">Type</option>
                     <option value="2">1.2 G M/T</option>
                     <option value="3">1.2 G CVT</option>
                  </select>
               </div>
               <div class="find_item">
                  <select class="nice_select">
                     <option value="1">DP</option>
                     <option value="2">25%</option>
                     <option value="3">30%</option>
                     <option value="3">35%</option>
                  </select>
               </div>
               <div class="find_item">
                  <select class="nice_select">
                     <option value="1">Tenor</option>
                     <option value="2">12X - 1 Tahun</option>
                     <option value="2">24X - 2 Tahun</option>
                     <option value="2">36X - 3 Tahun</option>
                     <option value="2">48X - 4 Tahun</option>
                     <option value="2">60X - 5 Tahun</option>
                  </select>
               </div>
               <div class="find_item">
                  <input type="text" class="form-control" placeholder="Isi DP Anda">
                  </div>
                
            </div>
            <div class="find_form_inner row m0" style="padding-top:3px;">
               <div class="find_item">
                  <button class="submit_btn" type="button"><i class="fa fa-calculator"></i> Hitung</button>
               </div>
            </div>
         </div>
      </section>
      <section class="car_about_area truck_video pad_btm wow animated fadeInUp" data-wow-delay="0.2s" style="padding-top:30px;">
         <div class="container">
            <div class="row car_about_inner">
               <div class="col-lg-6">
                  <div class="c_ab_text">
                     <h6>Tentang Kami</h6>
                     <h5>TOYOTA SPEKTAKULER</h5>
                     <p class="text-justify">
                           "TOYOTA SPEKTAKULER, Dapatkan Mobil Impian Anda dengan Harga Terbaik!
                           Sambut kesempatan langka ini dan miliki mobil Toyota favorit Anda. 
                           Toyota memberikan pengalaman berkendara yang luar biasa.</p>
                     <p class="text-justify">
                           Alan Nasmoco, sales kami yang berdedikasi, 
                           siap membantu Anda memilih mobil yang sesuai dengan kebutuhan dan anggaran Anda. Jangan sia-siakan kesempatan ini, 
                           hubungi Alan di +62 818-0211-0288 atau kunjungi showroom Toyota terdekat sekarang! Nikmati kenyamanan, keamanan, 
                           dan prestise berkendara bersama Toyota. Ayo, raih kesempurnaan berkendara dengan Toyota bersama Alan,
                           dan buat mimpi Anda menjadi kenyataan!"
                     </p>
                        <blockquote class="blockquote">
                        <p><i class="icon-quote"></i>"Temukan kesempurnaan berkendara dengan Toyota - Solusi terbaik untuk mobilitas Anda!"</p>
                        <h4>Alan Nasmoco</h4>
                     </blockquote>
                     <img class="sing" src="img/signature.png" alt>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="car_video">
                     <img class="img-fluid" src="img/Alan - Nasmoco.png" alt>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="market_place_area p_100">
         <div class="container">
            <div class="row market_inner">
               <div class="col-lg-6">
                  <div class="car_img">
                     <img class="img-fluid wow animated fadeInLeft" src="img/katalog/banner-inova.png" alt>
                     <ul class="nav">
                        <li class="wow animated fadeInLeft" data-wow-delay="0.2s">
                           <h4>50+</h4>
                           <h5>Sold</h5>
                        </li>
                        <li class="wow animated fadeInDown" data-wow-delay="0.4s">
                           <h4>20+</h4>
                           <h5>kmph</h5>
                        </li>
                        <li class="wow animated fadeInRight" data-wow-delay="0.5s">
                           <h4>10+</h4>
                           <h5>Awards</h5>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="market_text">
                     <h3>INNOVA ZENIX <span><br> Mulai Rp.</span> 383.000.000</h3>
                     <div class="row maket_item_inner">
                        <div class="col-sm-6">
                           <div class="media">
                              <div class="d-flex">
                                 <i class="icon-car_2"></i>
                              </div>
                              <div class="media-body">
                                 <h4>Fitur 1</h4>
                                 <p>New 10" Head Unit with Smartphone Connectivity</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="media">
                              <div class="d-flex">
                                 <i class="icon-pricing"></i>
                              </div>
                              <div class="media-body">
                                 <h4>Fitur 2</h4>
                                 <p>New Power Backdoor with Voice Command</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="media">
                              <div class="d-flex">
                                 <i class="icon-car_shield"></i>
                              </div>
                              <div class="media-body">
                                 <h4>Fitur 3</h4>
                                 <p>New Captain Seat with Ottoman</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="media">
                              <div class="d-flex">
                                 <i class="icon-rating"></i>
                              </div>
                              <div class="media-body">
                                 <h4>Fitur 4</h4>
                                 <p>New 10" Dual Rear Seat Entertainment</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="media">
                              <div class="d-flex">
                                 <i class="icon-car_shield"></i>
                              </div>
                              <div class="media-body">
                                 <h4>Fitur 5</h4>
                                 <p>New 10" Dual Rear Seat Entertainment</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="media">
                              <div class="d-flex">
                                 <i class="icon-rating"></i>
                              </div>
                              <div class="media-body">
                                 <h4>Fitur 6</h4>
                                 <p>New TNGA Platform</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
     <!-- kelebihan -->
     <section class="quickly_area">
         <div class="container">
            <div class="quickly_inner">
               <h3 class="wow animated fadeInUp" data-wow-delay="0.2s">Kelebihan <span>Memilih </span> Kami !</h3>
            </div>
            <div class="row b_feature_inner">
               <div class="col-lg-3 col-sm-6 ">
                  <div class="b_f_item wow animated fadeInLeft" data-wow-delay="0.2s">
                     <center>
                        <div class="icon">
                        <i class="icon-support1"></i>
                        </div>
                     </center>
                     <a href="#">
                        <h4>Konsultasi Pembelian</h4>
                     </a>
                     <p class="text-justify">Sales Toyota siap memberikan konsultasi pribadi untuk membantu Anda menemukan mobil Toyota yang sesuai dengan kebutuhan dan preferensi Anda</p>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6 ">
                  <div class="b_f_item wow animated fadeInLeft" data-wow-delay="0.4s">
                    <center>
                     <div class="icon">
                           <i class="icon-car"></i>
                        </div>
                    </center>
                     <a href="#">
                        <h4>Test Drive</h4>
                     </a>
                     <p class="text-justify">Nikmati kesempatan untuk menguji mobil Toyota pilihan Anda, merasakan performanya, dan mengalami kenyamanannya melalui uji coba kendaraan yang disesuaikan dengan jadwal Anda</p>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6 ">
                  <div class="b_f_item wow animated fadeInLeft" data-wow-delay="0.6s">
                     <center>
                        <div class="icon">
                           <i class="icon-calculator"></i>
                        </div>
                     </center>
                     <a href="#">
                        <h4>Simulasi Kredit</h4>
                     </a>
                     <p class="text-justify">Dapatkan simulasi kredit yang transparan dan akurat untuk memperkirakan cicilan bulanan dan persyaratan pembiayaan Anda, membantu Anda membuat keputusan yang tepat dalam memilih mobil Toyota yang sesuai dengan anggaran Anda.</p>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6 ">
                  <div class="b_f_item wow animated fadeInLeft" data-wow-delay="0.8s">
                     <center>
                        <div class="icon">
                           <i class="icon-free_delivery"></i>
                        </div>
                     </center>
                     <a href="#">
                        <h4>Konsultasi Dari Rumah</h4>
                     </a>
                     <p class="text-justify">Kami menyediakan layanan konsultasi di rumah yang nyaman dan fleksibel. Sales Toyota kami akan datang langsung ke rumah Anda untuk membahas opsi mobil, fitur-fitur, pembiayaan, dan menjawab pertanyaan Anda, memberikan kemudahan dan kenyamanan dalam memilih mobil Toyota.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="">
         <div class="container">
            <div class="flag_center_title">
               <h3 class="wow animated fadeInUp" data-wow-delay="0.2s" style="padding-top:30px;"> Pilih Tipe Mobil Anda</h3>
            </div>
            <div class="car_browse_slider owl-carousel" style="padding-bottom:30px;">
               <div class="item">
                  <div class="car_c_item">
                     <a href="pricelist.php"><img src="img/jenis-mobil/MPV.png" alt></a>
                     <a href="pricelist.php">
                        <h5>MPV</h5>
                     </a>
                  </div>
               </div>
               <div class="item">
                  <div class="car_c_item">
                     <a href="pricelist.php"><img src="img/jenis-mobil/SUV.png" alt></a>
                     <a href="pricelist.php">
                        <h5>SUV</h5>
                     </a>
                  </div>
               </div>
               <div class="item">
                  <div class="car_c_item">
                     <a href="pricelist.php"><img src="img/jenis-mobil/Hatchback.png" alt></a>
                     <a href="pricelist.php">
                        <h5>Hatchback</h5>
                     </a>
                  </div>
               </div>
               <div class="item">
                  <div class="car_c_item">
                     <a href="pricelist.php"><img src="img/jenis-mobil/commercial.png" alt></a>
                     <a href="pricelist.php">
                        <h5>Commercial</h5>
                     </a>
                  </div>
               </div>
               <div class="item">
                  <div class="car_c_item">
                     <a href="pricelist.php"><img src="img/jenis-mobil/sedan.png" alt></a>
                     <a href="pricelist.php">
                        <h5>Sedan</h5>
                     </a>
                  </div>
               </div>
               <div class="item">
                  <div class="car_c_item">
                     <a href="pricelist.php"><img src="img/jenis-mobil/sport.png" alt></a>
                     <a href="pricelist.php">
                        <h5>Sport </h5>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>
    <?php include 'katalog.php' ?>
   <section class="latest_blog_area p_100">
         <div class="container">
            <div class="flag_center_title wow animated fadeInUp" data-wow-delay="0.2s">
               <h2>Tips toyotajogja.id</h2>
            </div>
            <div class="row l_blog_inner">
               <div class="col-lg-4 col-sm-6">
                  <div class="l_blog_item wow animated fadeInDown" data-wow-delay="0.2s">
                     <div class="blog_img">
                        <a href="detail_info.php"><img class="img-fluid" src="img/info/info1.jpg" alt></a>
                        <a class="cat" href="#">Design</a>
                     </div>
                     <div class="blog_content">
                        <a class="date" href="#"><i class="icon-calendar1"></i> 20 Juli 2023</a>
                        <a href="detail_info.php">
                           <h6>Nikmati Low Rate Dari Toyota</h6>
                        </a>
                       
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6">
                  <div class="l_blog_item wow animated fadeInDown" data-wow-delay="0.4s">
                     <div class="blog_img">
                        <a href="detail_info.php"><img class="img-fluid" src="img/info/info2.jpg" alt></a>
                        <a class="cat" href="#">Design</a>
                     </div>
                     <div class="blog_content">
                        <a class="date" href="#"><i class="icon-calendar1"></i> 20 Juli 2023</a>
                        <a href="detail_info.php">
                           <h6>#Bebas Ribet Ala Kinto</h6>
                        </a>
                       
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-sm-6">
                  <div class="l_blog_item wow animated fadeInDown" data-wow-delay="0.6s">
                     <div class="blog_img">
                        <a href="detail_info.php"><img class="img-fluid" src="img/info/info3.jpg" alt></a>
                        <a class="cat" href="#">Design</a>
                     </div>
                     <div class="blog_content">
                        <a class="date" href="#"><i class="icon-calendar1"></i> 20 Juli 2023</a>
                        <a href="detail_info.php">
                           <h6>Solusi Perawatan Berkala Ala Toyota</h6>
                        </a>
                       
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> 
      <section class="testimonials_area red2 p_100">
         <div class="container">
            <div class="title_two text-center white red2 wow  fadeInUp animated" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
               <h5>Kata Konsumen Kami</h5>
               <h2>Testimoni!</h2>
            </div>
            <div class="testi_inner">
               <div class="testi_slider owl-carousel owl-loaded owl-drag">
                  <div class="owl-stage-outer">
                     <div class="owl-stage" style="transform: translate3d(-1520px, 0px, 0px); transition: all 1.5s ease 0s; width: 5700px;">
                        <div class="owl-item cloned" style="width: 380px;">
                           <div class="item" >
                              <div class="testi_item" style="background-color:#978181b3;">
                                 <img src="img/testimonials/testimoni1.jpg" alt="">
                                 <p class="text-justify text-white">Layanan sales Toyota sangat memuaskan. Mereka mendengarkan kebutuhan saya dengan sabar dan memberikan saran yang tepat. Uji coba kendaraan membantu saya memilih mobil dengan keyakinan. Saya sangat puas dengan layanan purna jual yang baik.</p> 
                                 <div class="media">
                                    <div class="d-flex">
                                       <img class="rounded-circle" src="img/testimonials/testi-1.png" alt="">
                                    </div>
                                    <div class="media-body">
                                       <h3 class="text-white">Bastiar - Sedayu</h3>
                                       <h5 class="text-white">Konsumen - Alan Nasmoco</h5>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="owl-item active" style="width: 380px;">
                           <div class="item" >
                              <div class="testi_item" style="background-color:#978181b3;">
                                 <img src="img/testimonials/testimoni1.jpg" alt="">
                                 <p class="text-justify text-white">Sales mobil Toyota sangat profesional dan ramah. Mereka membantu saya dalam memilih mobil yang sesuai dengan anggaran saya dan memberikan penawaran khusus yang menguntungkan. Simulasi kredit sangat membantu dalam perencanaan pembayaran.</p> 
                                 <div class="media">
                                    <div class="d-flex">
                                       <img class="rounded-circle" src="img/testimonials/testi-1.png" alt="">
                                    </div>
                                    <div class="media-body">
                                       <h3 class="text-white">Rangga - Bantul</h3>
                                       <h5 class="text-white">Konsumen - Alan Nasmoco</h5>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="owl-item active" style="width: 380px;">
                           <div class="item" >
                              <div class="testi_item" style="background-color:#978181b3;">
                                 <img src="img/testimonials/testimoni1.jpg" alt="">
                                 <p class="text-justify text-white">Pelayanan sales mobil Toyota luar biasa. Mereka memberikan konsultasi yang detail dan jelas tentang fitur mobil. Uji coba kendaraan memberikan pengalaman langsung. Simulasi kredit membantu proses pembiayaan. Saya sangat puas dan akan merekomendasikannya.</p> 
                                 <div class="media">
                                    <div class="d-flex">
                                       <img class="rounded-circle" src="img/testimonials/testi-1.png" alt="">
                                    </div>
                                    <div class="media-body">
                                       <h3 class="text-white">Prastowo - Kasongan</h3>
                                       <h5 class="text-white">Konsumen - Alan Nasmoco</h5>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button></div>
                  <div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot"><span></span></button></div>
               </div>
            </div>
         </div>
      </section>
      <section class="p_100">
         <div class="container">
            <center>
               <div class="row">
                  <div class="col-sm-3">
                     <a href="#"  class="text-center"><img class=" wow animated fadeInLeft mb-2" src="img/logo/logo-l-toyotajogja.png"></a>
                  </div>
                  <div class="col-sm-3">
                     <a href="#"  class="text-center"><img class=" wow animated fadeInLeft mb-2" src="img/logo/logo-l-toyotajogja.png"></a>
                  </div>
                  <div class="col-sm-3">
                     <a href="#"  class="text-center"><img class=" wow animated fadeInLeft mb-2" src="img/logo/logo-l-toyotajogja.png"></a>
                  </div>
                  <div class="col-sm-3">
                     <a href="#"  class="text-center"><img class=" wow animated fadeInLeft mb-2" src="img/logo/logo-l-toyotajogja.png"></a>
                  </div>
               </div>
            </center>
         </div>
      </section>      
      <footer class="footer_area red_footer_area budget_footer">
         <div class="footer_widgets">
            <div class="container">
               <div class="row f_widgets_inner">
                  <div class="col-lg-3 col-sm-6">
                     <aside class="f_widget about_widget">
                        <a class="f_logo wow animated fadeInUp" data-wow-delay="0.2s" href="index.php" style="background-color:#eb0a1e;">
                           <center><img src="img/logo/logo-toyotajogja-2.png" srcset="img/footer-red-logo-2x.png 2x" alt="bar"></center>
                        </a>
                        <p><i class="icon-map_marker_2" aria-hidden="true"></i> Jl. Ringroad Timur No.58A, Sorowajan, Banguntapan, Kec. Banguntapan, Kabupaten Bantul, Daerah Istimewa Yogyakarta 55198</p>
                        <a href="https://toyotajogja.id"><i class="icon-envelop_2" aria-hidden="true"></i></a>
                        <a href="tel:0951856558"><i class="icon-phone_2" aria-hidden="true"></i> +62 818-0211-0288 (Sales Toyota)</a>
                     </aside>
                  </div>
                  <div class="col-lg-2 col-sm-6">
                     <aside class="f_widget link_widget wow animated fadeInUp" data-wow-delay="0.4s">
                        <div class="f_title">
                           <h3>Our info</h3>
                        </div>
                        <ul class="nav flex-column">
                           <li><a href="#">About us</a></li>
                           <li><a href="#">Our Services</a></li>
                           <li><a href="#">Our Shop</a></li>
                           <li><a href="#">Blog</a></li>
                           <li><a href="#">Contact us</a></li>
                        </ul>
                     </aside>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                     <aside class="f_widget link_widget wow animated fadeInUp" data-wow-delay="0.6s">
                        <div class="f_title">
                           <h3>Quick Link</h3>
                        </div>
                        <ul class="nav flex-column">
                           <li><a href="#">Private Policy</a></li>
                           <li><a href="#">Team & Conditions</a></li>
                           <li><a href="#">Copyright Notification</a></li>
                           <li><a href="#">Private Policy</a></li>
                           <li><a href="#">Team & Conditions</a></li>
                        </ul>
                     </aside>
                  </div>
                  <div class="col-lg-4 col-sm-6">
                     <aside class="f_widget subscribe_widget wow animated fadeInUp" data-wow-delay="0.8s">
                        <div class="f_title">
                           <h3>Follow Sosial Media Kami</h3>
                        </div>
                        <ul class="nav">
                           <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                           <li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
                        </ul>
                     </aside>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer_copyright">
            <div class="container">
               <div class="copyright_inner d-flex justify-content-between">
                  <div class="left">
                     <p>
                        @Copyright 2023 Reserved by <a href="index.php">toyotajogja.id</a> 
                     </p>
                  </div>
                  <div class="right">
                     <ul class="nav">
                        <li><a href="#">Terms of use</a></li>
                        <li><a href="#">Privacy Environmental</a></li>
                        <li><a href="#">Policy</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <div class="cart_sidebar">
         <div class="cart_sidebar_inner">
            <div class="sidebar_top">
               <div class="top_bar d-flex justify-content-between">
                  <img src="img/logo/logo-l-toyotajogja.png" alt="">
                  <div class="cart_close_btn">
                     <p><i class="fa fa-times"></i></p>
                  </div>
               </div>
               <div class="cart_side_list">
                  <div class="media">
                     <div class="d-flex"><i class="fa fa-whatsapp fa-3x"></i></div>
                     <div class="media-body">
                        <h4>+62 818-0211-0288</h4>
                        <p>Alan Toyota<span> Nasmoco</span> </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="sidebar_bottom">
               <a class="main_btn red border" href="#"><i class="fa fa-car"></i> Ajukan Kredit</a>
               <a class="main_btn red" href="#"><i class="fa fa-comments"></i> Konsultasi</a>
               
               
            </div>
         </div>
      </div>
      <div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
         <div class="search_box_inner">
            <h3>Search</h3>
            <div class="input-group">
               <input type="text" class="form-control" placeholder="Enter search keywords">
               <span class="input-group-btn">
               <button class="btn btn-default" type="button"><i class="icon icon-Search"></i></button>
               </span>
            </div>
         </div>
      </div>
      <script src="js/jquery-3.4.1.min.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="vendors/slick/slick.min.js"></script>
      <script src="vendors/datetimepicker/moment.js"></script>
      <script src="vendors/datetimepicker/tempusdominus-bootstrap-4.min.js"></script>
      <script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
      <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
      <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
      <script src="vendors/isotope/isotope.pkgd.min.js"></script>
      <script src="vendors/popup/jquery.magnific-popup.min.js"></script>
      <script src="vendors/animate-css/wow.min.js"></script>
      <script src="js/theme-dist.js"></script>
      <script defer src="https://static.cloudflareinsights.com/beacon.min.js/v52afc6f149f6479b8c77fa569edb01181681764108816" integrity="sha512-jGCTpDpBAYDGNYR5ztKt4BQPGef1P0giN6ZGVUi835kFF88FOmmn8jBQWNgrNd8g/Yu421NdgWhwQoaOPFflDw==" data-cf-beacon='{"rayId":"7dd383a81b923d83","version":"2023.4.0","r":1,"b":1,"token":"327f02abe4ab496bb762653489d2ae1d","si":100}' crossorigin="anonymous"></script>
   </body>
</html>