<!doctype html>
<html lang="en">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="icon" href="img/favicon-red.png" type="image/png">
      <title>Toyotajogja.id - Marketing Toyota Jogja</title>
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="vendors/icomoon-icon/style.css">
      <link rel="stylesheet" href="vendors/themify-icon/themify-icons.css">
      <link rel="stylesheet" href="vendors/datetimepicker/tempusdominus-bootstrap-4.min.css">
      <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
      <link rel="stylesheet" href="vendors/owl-carousel/assets/owl.carousel.min.css">
      <link rel="stylesheet" href="vendors/slick/slick-theme.css">
      <link rel="stylesheet" href="vendors/slick/slick.css">
      <link rel="stylesheet" href="vendors/animation/animate.css">
      <link rel="stylesheet" href="vendors/popup/magnific-popup.css">
      <link rel="stylesheet" href="vendors/animate-css/animate.css">
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="css/responsive.css">
   </head>
   <body data-scroll-animation="true">
      <div class="preloader">
         <div class="main-loader">
            <span class="loader1"></span>
            <span class="loader2"></span>
            <span class="loader3"></span>
         </div>
      </div>
     <!-- menu -->
     <header class="header_area menu_four">
        <div class="top_bus_menu">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div class="left">
                    <h6><a href=""><i class="icon-clock"></i>Senin-Sabtu 08:00 - 16:00 WIB</h6>
                    </div>
                    <div class="right">
                        <a href="#"><i class="fa fa-whatsapp fa-2x"></i><span>+62</span> 818-0211-0288<span></span></a>
                        <p><i class="icon-map_marker"></i> Kasihan, Bantul, DIY</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand" href="index.php"><img src="img/logo/logo-l-toyotajogja.png" srcset="img/bus-logo-2x.png 2x" alt="bar"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="active">
                           <a href="index.php">Beranda</a>
                        </li>
                        <li class="dropdown submenu">
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Mobil</a>
                           <ul class="dropdown-menu">
                              <li><a href="pricelist.php">Hatchback</a></li>
                              <li><a href="pricelist.php">MPV</a></li>
                              <li><a href="pricelist.php">SUV</a></li>
                              <li><a href="pricelist.php">Sedan</a></li>
                              <li><a href="pricelist.php">Sport</a></li>
                              <li><a href="pricelist.php">Commercial</a></li>
                           </ul>
                        </li>
                        <li><a href="pricelist.php">PRICELIST</a></li>
                        <li><a href="promo.php">PROMO</a></li>
                        <li><a href="info.php">Berita</a></li>
                        <li><a href="#" class="cart_bag_btn">Kontak</a></li>
                     </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <section class="breadcrumb_area" style="background: url(img/breadcrumb/brudcum.jpg)">
         <div class="container">
            <div class="breadcrumb_inner">
               <h3>Informasi Tips & Trik Mobil</h3>
               <div class="link">
                  <a href="index.php">Home</a>
                  <a href="#">Berita</a>
               </div>
            </div>
         </div>
      </section>
      <section class="blog_area p_100">
         <div class="container">
            <div class="row blog_grid_inner">
            <div class="col-lg-8">
                  <div class="single_blog_inner">
                     <article class="blog_item">
                        <div class="blog_img">
                           <img class="img-fluid" src="img/info/info-toyotajogja3.jpg" alt>
                        </div>
                        <div class="blog_text">
                           <div class="cat_area">
                              <div class="cat_list">
                                 <a href="#">Design</a>
                              </div>
                              <a href="#"><i class="icon-calendar1"></i>Nov 20, 2023</a>
                              <a href="#"><i class="icon-user-o"></i>By Ridoy</a>
                           </div>
                           <a href="detail_info.php">
                              <h3>Main reasons to buy luxury car in 2018</h3>
                           </a>
                           <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed them eiusmod. tempor incididunt is labore et dolorie magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. ullamco labo ris nisi ut aliquip ex ea commodo consequat. </p>
                           <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam</p>
                           <blockquote class="blockquote">
                              <p>Social media change the relationship between them also companies .and cust omers from masters to servants and many more to come in the end...</p>
                              <h4>Rodney Artichoke</h4>
                           </blockquote>
                           <div class="single_view">
                              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam</p>
                              <img class="img-fluid" src="img/info/info-toyotajogja1.jpg" alt>
                              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam</p>
                           </div>
                           <div class="tags">
                              <h6>Tags:</h6>
                              <ul class="nav">
                                 <li><a href="#">App & Saas</a></li>
                                 <li><a href="#">UI design</a></li>
                                 <li><a href="#">Saas</a></li>
                              </ul>
                           </div>
                           <div class="single_social">
                              <h5>Share</h5>
                              <a href="#"><i class="fa fa-facebook"></i></a>
                              <a href="#"><i class="fa fa-youtube"></i></a>
                              <a href="#"><i class="fa fa-twitter"></i></a>
                              <a href="#"><i class="fa fa-dribbble"></i></a>
                           </div>
                        </div>
                     </article>
                     <div class="author_details">
                        <div class="media">
                           <div class="d-flex">
                              <img class="rounded-circle" src="img/comment/author-1.png" alt>
                           </div>
                           <div class="media-body">
                              <a href="#">
                                 <h4>Alan - Nasmoco</h4>
                              </a>
                              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore euro fugiat nulla pariatur. Excepteur sint occaecat cupidatat</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="right_sidebar_area">
                     <aside class="r_widget search_widget">
                        <div class="input-group">
                           <input type="text" class="form-control" placeholder="Search...">
                           <div class="input-group-append">
                              <button class="btn" type="button"><i class="icon-search1"></i></button>
                           </div>
                        </div>
                     </aside>
                     <aside class="r_widget news_widget">
                        <div class="r_title">
                           <h3>Latest News</h3>
                        </div>
                        <div class="news_inner">
                           <div class="media">
                              <div class="d-flex">
                                 <img src="img/blog/l-news/l-news-1.jpg" alt>
                              </div>
                              <div class="media-body">
                                 <a href="#">
                                    <h4>The harder you work them more luck.</h4>
                                 </a>
                                 <a href="#">
                                    <p><i class="icon-calendar1"></i>March 14, 2019</p>
                                 </a>
                              </div>
                           </div>
                           <div class="media">
                              <div class="d-flex">
                                 <img src="img/blog/l-news/l-news-2.jpg" alt>
                              </div>
                              <div class="media-body">
                                 <a href="#">
                                    <h4>Bartosz Ostalowski is a pro with his feet</h4>
                                 </a>
                                 <a href="#">
                                    <p><i class="icon-calendar1"></i>March 14, 2019</p>
                                 </a>
                              </div>
                           </div>
                           <div class="media">
                              <div class="d-flex">
                                 <img src="img/blog/l-news/l-news-3.jpg" alt>
                              </div>
                              <div class="media-body">
                                 <a href="#">
                                    <h4>Fiat Chryler senior for manager charged in</h4>
                                 </a>
                                 <a href="#">
                                    <p><i class="icon-calendar1"></i>March 14, 2019</p>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </aside>
                     <aside class="r_widget categories_widget">
                        <div class="r_title">
                           <h3>Kategori</h3>
                        </div>
                        <ul class="list">
                           <li><a href="#"><i class="icon-caret-right"></i>Tips <span>(16)</span></a></li>
                           <li><a href="#"><i class="icon-caret-right"></i>Perawatan <span>(20)</span></a></li>
                           <li><a href="#"><i class="icon-caret-right"></i>Teknologi <span>(10)</span></a></li>
                           <li><a href="#"><i class="icon-caret-right"></i>Mobil <span>(12)</span></a></li>
                           <li><a href="#"><i class="icon-caret-right"></i>Interior <span>(5)</span></a></li>
                           <li><a href="#"><i class="icon-caret-right"></i>Eksterior <span>(14)</span></a></li>
                        </ul>
                     </aside>
                    
                     <aside class="r_widget tag_widget">
                        <div class="r_title">
                           <h3>Tags</h3>
                        </div>
                        <ul class="nav">
                           <li><a href="#">Exteriors</a></li>
                           <li><a href="#">Design</a></li>
                           <li><a href="#">Cars</a></li>
                           <li><a href="#">Cooling System </a></li>
                           <li><a href="#">Interiors </a></li>
                           <li><a href="#">Marketing</a></li>
                           <li><a href="#">Speeds</a></li>
                           <li><a href="#">2024</a></li>
                           <li><a href="#">Software</a></li>
                           <li><a href="#">Concept car</a></li>
                        </ul>
                     </aside>
                    
                  </div>
               </div>
            </div>
         </div>
      </section>
      <footer class="footer_area red_footer_area budget_footer">
         <div class="footer_widgets">
            <div class="container">
               <div class="row f_widgets_inner">
                  <div class="col-lg-3 col-sm-6">
                     <aside class="f_widget about_widget">
                        <a class="f_logo wow animated fadeInUp" data-wow-delay="0.2s" href="index.php" style="background-color:#eb0a1e;">
                           <center><img src="img/logo/logo-toyotajogja-2.png" srcset="img/footer-red-logo-2x.png 2x" alt="bar"></center>
                        </a>
                        <p><i class="icon-map_marker_2" aria-hidden="true"></i> Jl. Ringroad Timur No.58A, Sorowajan, Banguntapan, Kec. Banguntapan, Kabupaten Bantul, Daerah Istimewa Yogyakarta 55198</p>
                        <a href="https://toyotajogja.id"><i class="icon-envelop_2" aria-hidden="true"></i></a>
                        <a href="tel:0951856558"><i class="icon-phone_2" aria-hidden="true"></i> +62 818-0211-0288 (Sales Toyota)</a>
                     </aside>
                  </div>
                  <div class="col-lg-2 col-sm-6">
                     <aside class="f_widget link_widget wow animated fadeInUp" data-wow-delay="0.4s">
                        <div class="f_title">
                           <h3>Our info</h3>
                        </div>
                        <ul class="nav flex-column">
                           <li><a href="#">About us</a></li>
                           <li><a href="#">Our Services</a></li>
                           <li><a href="#">Our Shop</a></li>
                           <li><a href="#">Blog</a></li>
                           <li><a href="#">Contact us</a></li>
                        </ul>
                     </aside>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                     <aside class="f_widget link_widget wow animated fadeInUp" data-wow-delay="0.6s">
                        <div class="f_title">
                           <h3>Quick Link</h3>
                        </div>
                        <ul class="nav flex-column">
                           <li><a href="#">Private Policy</a></li>
                           <li><a href="#">Team & Conditions</a></li>
                           <li><a href="#">Copyright Notification</a></li>
                           <li><a href="#">Private Policy</a></li>
                           <li><a href="#">Team & Conditions</a></li>
                        </ul>
                     </aside>
                  </div>
                  <div class="col-lg-4 col-sm-6">
                     <aside class="f_widget subscribe_widget wow animated fadeInUp" data-wow-delay="0.8s">
                        <div class="f_title">
                           <h3>Follow Sosial Media Kami</h3>
                        </div>
                        <ul class="nav">
                           <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                           <li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
                        </ul>
                     </aside>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer_copyright">
            <div class="container">
               <div class="copyright_inner d-flex justify-content-between">
                  <div class="left">
                     <p>
                        @Copyright 2023 Reserved by <a href="index.php">toyotajogja.id</a> 
                     </p>
                  </div>
                  <div class="right">
                     <ul class="nav">
                        <li><a href="#">Terms of use</a></li>
                        <li><a href="#">Privacy Environmental</a></li>
                        <li><a href="#">Policy</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <div class="cart_sidebar">
         <div class="cart_sidebar_inner">
            <div class="sidebar_top">
               <div class="top_bar d-flex justify-content-between">
                  <img src="img/logo/logo-l-toyotajogja.png" alt="">
                  <div class="cart_close_btn">
                     <p><i class="fa fa-times"></i></p>
                  </div>
               </div>
               <div class="cart_side_list">
                  <div class="media">
                     <div class="d-flex"><i class="fa fa-whatsapp fa-3x"></i></div>
                     <div class="media-body">
                        <h4>+62 818-0211-0288</h4>
                        <p>Alan Toyota<span> Nasmoco</span> </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="sidebar_bottom">
               <a class="main_btn red border" href="#"><i class="fa fa-car"></i> Ajukan Kredit</a>
               <a class="main_btn red" href="#"><i class="fa fa-comments"></i> Konsultasi</a>
               
               
            </div>
         </div>
      </div>
      <div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
         <div class="search_box_inner">
            <h3>Search</h3>
            <div class="input-group">
               <input type="text" class="form-control" placeholder="Enter search keywords">
               <span class="input-group-btn">
               <button class="btn btn-default" type="button"><i class="icon icon-Search"></i></button>
               </span>
            </div>
         </div>
      </div>
      <script src="js/jquery-3.4.1.min.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="vendors/slick/slick.min.js"></script>
      <script src="vendors/datetimepicker/moment.js"></script>
      <script src="vendors/datetimepicker/tempusdominus-bootstrap-4.min.js"></script>
      <script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
      <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
      <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
      <script src="vendors/isotope/isotope.pkgd.min.js"></script>
      <script src="vendors/popup/jquery.magnific-popup.min.js"></script>
      <script src="vendors/animate-css/wow.min.js"></script>
      <script src="js/theme-dist.js"></script>
   </body>
</html>