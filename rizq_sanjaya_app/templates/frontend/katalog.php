<section class="latest_collection_area p_100">
         <div class="container">
            <div class="flag_title">
               <h2 class="wow animated fadeInUp" data-wow-delay="0.2s"><i class="icon-car_2"></i> Katalog</h2>
            </div>
            <div class="row l_collection_inner">
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="0.2s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/yaris/yaris-black.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="0.4s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/Corola/corola.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="0.6s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/inova/inova.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="0.8s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/Corola/corola-altis.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1.2s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/Corola/BZ4X.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/Corola/camry.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/calya/calya.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
                <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/velos/velos.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/inova/inovaold.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/avanza/avanza.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/foxy/foxy.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/alphard/alphard.png" alt></a></div>
                     <div class="text_body">
                     <br>
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                         
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <br>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/vellfire/vellfire.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/raize/raize.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/fortuner/fortuner.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/rush/rush.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/landcruiser/landcruiser.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/yaris/yaris-old.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/agya/agya.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/vios/vios.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/hilux/hilux-box.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/hilux/hilux-g-sport.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/hilux/hilux-d-cap.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/hilux/hilux-s-cap.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/hiace/hiace.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/hiace/hiace-2.png" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-6">
                  <div class="l_collection_item wow animated fadeInUp" data-wow-delay="1s">
                     <div class="car_img"><a href="pricelist.php"><img class="img-fluid" src="img/katalog/dyna/dyna-merah" alt></a></div>
                     <div class="text_body">
                        <a href="pricelist.php">
                           <h4>Nama Mobil</h4>
                        </a>
                        <h5>Rp. 300 Juta</h5>
                        <p><i class="fa fa-calculator"></i> Simulasikan Kredit</p>
                     </div>
                     <div class="text_footer">
                        <a href="#"><i class="icon-engine"></i> 2500</a>
                        <a href="#"><i class="icon-gear1"></i> Manual</a>
                        <a href="#"><i class="icon-oil"></i>20/24</a>
                     </div>
                  </div>
               </div>
               
               <a class="main_btn red wow animated fadeInRight" data-wow-delay="0.2s" href="#"><i class="icon-car_2"></i> Lihat Semua</a>
            </div>
         </div>
      </section>