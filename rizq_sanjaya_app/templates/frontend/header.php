 <header class="header_area menu_four">
        <div class="top_bus_menu">
            <div class="container">
                <div class="d-flex justify-content-between">
                    <div class="left">
                    <h6><a href=""><i class="icon-clock"></i>Senin-Sabtu 08:00 - 16:00 WIB</h6>
                    </div>
                    <div class="right">
                        <a href="#"><i class="fa fa-whatsapp fa-2x"></i><span>+62</span> 822 2094 9004<span></span></a>
                        <p><i class="icon-map_marker"></i> Kasihan, Bantul, DIY</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand" href="index.php"><img src="img/logo/logo-l-toyotajogja.png" srcset="img/bus-logo-2x.png 2x" alt="bar"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="active">
                           <a href="index.php">Beranda</a>
                        </li>
                        <li class="dropdown submenu">
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Mobil</a>
                           <ul class="dropdown-menu">
                              <li><a href="pricelist.php">Hatchback</a></li>
                              <li><a href="pricelist.php">MPV</a></li>
                              <li><a href="pricelist.php">SUV</a></li>
                              <li><a href="pricelist.php">Sedan</a></li>
                              <li><a href="pricelist.php">Sport</a></li>
                              <li><a href="pricelist.php">Commercial</a></li>
                           </ul>
                        </li>
                        <li><a href="pricelist.php">PRICELIST</a></li>
                        <li><a href="promo.php">PROMO</a></li>
                        <li><a href="info.php">Berita</a></li>
                        <li><a href="#" class="cart_bag_btn">Kontak</a></li>
                     </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>