from django import template
import locale

register = template.Library()

@register.filter
def lower(value):
    return value.lower()
    
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.filter
def batch(sequence, count):
    result = []
    for i in range(0, len(sequence), count):
        result.append(sequence[i:i + count])
    return result

@register.filter
def format_rupiah(value):
    if value:
        locale.setlocale(locale.LC_ALL, 'id_ID.UTF-8')
        return locale.currency(float(value), grouping=True)
    return value