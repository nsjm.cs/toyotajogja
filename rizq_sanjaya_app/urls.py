from django.urls import path, include
from .views import *
from django.contrib.auth import views as auth_views


app_name = 'profile'
urlpatterns = [
    path('', dashboard.index, name='home'),
    path('calculate_loan', dashboard.calculate_loan, name='calculate_loan'),

    #INI ADALAH YOGYAEXPLORE
    path('profile/', dashboard.profil, name='profil'),
    path('paket/', dashboard.paket, name='paket'),
    path('detail_paket/<int:id>', dashboard.detail_paket, name='detail_paket'),
    path('cara_bayar/', dashboard.cara_bayar, name='cara_bayar'),
    path('gallery/', dashboard.galeri, name='galeri'),
    path('berita/', dashboard.beritaMobil, name='berita'),
    path('detail_info/<str:slug>', dashboard.detail_info, name='detail_info'),
    path('detail_mobil/<str:id>', dashboard.detail_mobil, name='detail_mobil'),
    path('promo/', dashboard.promo, name='promo'),
    path('pricelist/', dashboard.pricelist, name='pricelist'),
    path('category/<int:id>', dashboard.kategori, name='kategori'),
    path('detail_berita/<str:slug>', dashboard.detail_berita, name='detail_berita'),
    path('contact/', dashboard.kontak, name='kontak'),

    # path('gallery/', dashboard.gallery, name='gallerry'),
    path('prev/<int:id>/', dashboard.prev, name='prev'),
    path('next/<int:id>/', dashboard.next, name='next'),
    path('Category/', dashboard.ajax, name='ajax'),
    path('Produk/<int:id>', dashboard.produk, name='produk'),
    path('contact_us/', dashboard.contact, name='contact'),
    path('supplier/', dashboard.supplier, name='supplier'),
    path('render_laporan/', dashboard.render_laporan, name='render_laporan'),
    # url(r'refresh/$', views.captcha_refresh, name='captcha-refresh')
    # path('captcha/refresh/', guestbook.captcha_refresh, name='captcha_refresh'),

    # ABOUT ==================
    path('about/', halaman.index_about, name='idx_about'),
    
    # Mobil ==================
    path('kategori_mobil/<int:id_kat>', mobil_front.index, name='kategori_mobil'),

    # KONTAK PAJERO SPORT ==================
    path('contact/', halaman.index_contact, name='idx_contact'),

    # === Layanan ========================
    path('layanan/', layanan.index, name='layanan'),
    path('layanan/<str:jenis>', layanan.detail, name='layanan'),
    path('layanan/komoditas/<str:jenis>', layanan.detail_komoditas, name='detail_komoditas'),

    # DOKUMEN PUBLIKASI
    # path('dokumen/', documents.index, name='document'),
    # path('dokumen/<str:slug>', documents.detail, name='document_detail'),
    
    # NEWS ========================================
    path('berita/', news.index, name='news'),
    path('berita/<str:slug>', news.detail, name='news_detail'),
    path('berita/category/<str:jenis>/<str:slug>', news.category, name='news_by_category'),

    # DOKUMEN PUBLIKASI
    path('galeri/', gallery.index, name='gallery'),
    path('galeri/<str:slug>', gallery.detail, name='gallery_detail'),
    # path('galeri/category/<str:slug>', gallery.category, name='gallery_by_category'),

    # PAGES
    # path('profil/<str:slug>', pages.index, name='pages'),

    # AGENDA
    # path('agenda/', agenda.index, name='agenda'),
    # path('api/agenda/get', agenda.get_agenda, name='agenda_get'),
    # path('agenda/<str:slug>', agenda.get_agenda_detail, name='agenda_detail'),

    # BUKU TAMU
    # path('buku_tamu/', guestbook.index, name='guestbook'),

    # PENGADUAN
    # path('complaint/', complaint.index, name='complaint'),

    # PENGUMUMAN
    # path('pengumuman/', announcement.index, name='announcement'),
    # path('pengumuman/<str:slug>', announcement.detail_front, name='announcement_detail'),

    # TAGS
    path('tags/<str:jenis>/<str:slug>', tags.index, name='tags'),

    # PASSWORD
    # path("password_reset", user.password_reset_request, name="password_reset"),
    # path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='profile/admin/user/password/password_reset_done.html'), name='password_reset_done'),
    # path('password/reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name="profile/admin/user/password/password_reset_confirm.html"), name='password_reset_confirm_custom'),

    # PATH URL UNTUK ADMIN 
    path('manage_panel/', include(
        [
            path('', admin.index, name='admin_home'),
            path('verification/', user.verification, name='admin_verification'),
            path('send_verification/', user.send_verification, name='admin_send_verification'),
            path('email/verify/<uidb64>/<token>/',user.email_verify, name='admin_email_verify'),

            # PRODUK =====================================
            path('product/', include(
                [
                    path('', product.admin_index, name='admin_product'),
                    path('add/', product.create, name='admin_product_create'),
                    path('edit/<str:slug>', product.edit, name='admin_product_edit'),
                    path('detail/<str:slug>', product.admin_detail, name='admin_product_detail'),
                    path('soft_delete/<str:slug>', product.softDelete, name='admin_product_soft_delete'),
                    path('permanent_delete/<str:slug>', product.permanentDelete, name='admin_product_permanent_delete'),
                    path('restore/<str:slug>', product.restore, name='admin_product_restore'),
                ]
            )),
            
            # NEWS BERITA
            path('news/', include(
                [
                    path('', news.admin_index, name='admin_news'),
                    path('create/', news.create, name='admin_news_create'),
                    path('edit/<str:slug>', news.edit, name='admin_news_edit'),
                    path('detail/<str:slug>', news.admin_detail, name='admin_news_detail'),
                    path('soft_delete/<str:slug>', news.softDelete, name='admin_news_soft_delete'),
                    path('permanent_delete/<str:slug>', news.permanentDelete, name='admin_news_permanent_delete'),
                    path('restore/<str:slug>', news.restore, name='admin_news_restore'),
                ]
            )),

            path('kata_mereka/', include(
                [
                    path('', kata.admin_index, name='admin_KM'),
                    path('create/', kata.create, name='admin_KM_create'),
                    path('edit/<int:id>', kata.edit, name='admin_KM_edit'),
                    path('detail/<int:id>', kata.admin_detail, name='admin_KM_detail'),
                    path('soft_delete/<int:id>', kata.softDelete, name='admin_KM_soft_delete'),
                    path('permanent_delete/<int:id>', kata.permanentDelete, name='admin_KM_permanent_delete'),
                    path('restore/<int:id>', kata.restore, name='admin_KM_restore'),
                ]
            )),
            path('Keunggulan/', include(
                [
                    path('', keunggulan.admin_index, name='admin_keunggulan'),
                    path('', keunggulan.admin_index, name='admin_keunggulan'),
                    path('create/', keunggulan.create, name='admin_keunggulan_create'),
                    path('edit/<int:id>', keunggulan.edit, name='admin_keunggulan_edit'),
                    path('detail/<int:id>', keunggulan.admin_detail, name='admin_keunggulan_detail'),
                    path('soft_delete/<int:id>', keunggulan.softDelete, name='admin_keunggulan_soft_delete'),
                    path('permanent_delete/<int:id>', keunggulan.permanentDelete, name='admin_keunggulan_permanent_delete'),
                    path('restore/<int:id>', keunggulan.restore, name='admin_keunggulan_restore'),
                ]
            )),
            path('kategori_mobil/', include(
                [
                    
                    path('', kategorimobil.admin_index, name='admin_kateogri_mobil'),
                    path('create/', kategorimobil.create, name='admin_kategorimobil_create'),
                    path('edit/<int:id>', kategorimobil.edit, name='admin_kategorimobil_edit'),
                    path('detail/<int:id>', kategorimobil.admin_detail, name='admin_kategorimobil_detail'),
                    path('soft_delete/<int:id>', kategorimobil.softDelete, name='admin_kategorimobil_soft_delete'),
                    path('permanent_delete/<int:id>', kategorimobil.permanentDelete, name='admin_kategorimobil_permanent_delete'),
                    path('restore/<int:id>', kategorimobil.restore, name='admin_kategorimobil_restore'),
                ]
            )),

            path('pricelist/', include(
                [
                    path('', pricelists.admin_index, name='admin_pricelist_mobil'),
                    path('create/', pricelists.create, name='admin_pricelist_create'),
                    path('edit/<int:id>/', pricelists.edit, name='admin_pricelist_edit'),
                    path('soft_delete/<int:id>', pricelists.softDelete, name='admin_pricelist_soft_delete'),
                    path('permanent_delete/<int:id>', pricelists.permanentDelete, name='admin_pricelist_permanent_delete'),
                    path('restore/<int:id>', pricelists.restore, name='admin_pricelist_restore'),
                ]
            )),

            path('mobil/', include(
                [
                    path('', mobil.admin_index, name='admin_mobil'),
                    path('create/', mobil.create, name='admin_mobil_create'),
                    path('edit/<int:id>', mobil.edit, name='admin_mobil_edit'),
                    path('detail/<int:id>', mobil.admin_detail, name='admin_mobil_detail'),
                    path('soft_delete/<int:id>', mobil.softDelete, name='admin_mobil_soft_delete'),
                    path('restore/<int:id>', mobil.restore, name='admin_mobil_restore'),
                    path('permanent_delete/<int:id>', mobil.permanentDelete, name='admin_mobil_delete'),
                    
                    path('child/', include(
                        [
                            path('index/<int:id>/', mobil.child_index, name='admin_child'),
                            path('create/<int:id>/', mobil.child_create, name='admin_child_create'),
                            path('edit/<int:id>/<str:child>', mobil.child_edit, name='admin_child_edit'),
                            path('permanent_delete_child/<int:id>/<str:child>/', mobil.permanentDeletechild, name='admin_child_permanent_delete'),
                        ]
                    )),
                ]
            )),
            
            # GALLERY
            # path('galeri/', include(
            #     [
            #         path('', gallery.admin_index, name='admin_gallery'),
            #         path('create/', gallery.create, name='admin_gallery_create'),
            #         path('edit/<str:slug>', gallery.edit, name='admin_gallery_edit'),
            #         path('detail/<str:slug>', gallery.admin_detail, name='admin_gallery_detail'),
            #         path('soft_delete/<str:slug>', gallery.softDelete, name='admin_gallery_soft_delete'),
            #         path('permanent_delete/<str:slug>', gallery.permanentDelete, name='admin_gallery_permanent_delete'),
            #         path('restore/<str:slug>', gallery.restore, name='admin_gallery_restore'),
            #     ]
            # )),

            # LAYANAN KAMI
            path('layanan/', include(
                [
                    path('', services.admin_index, name='admin_services'),
                    path('create/', services.create, name='admin_services_create'),
                    path('edit/<int:id>', services.edit, name='admin_services_edit'),
                    path('delete/<int:id>', services.Delete, name='admin_services_delete'),
                ]
            )),

            # # PAKET WISATA
            path('wisata/', include(
                [
                    path('', layanan.admin_index, name='admin_paket'),
                    path('create/', layanan.admin_create, name='admin_paket_create'),
                    path('edit/<int:id>', layanan.admin_edit, name='admin_paket_edit'),
                    path('delete/<int:id>', layanan.admin_Delete, name='admin_paket_delete'),

                    ### DETAIL PAKET =======================================================
                    path('fasilitas/<str:aksi>/<str:kode>/<int:id>', layanan.admin_fasilitas, name='admin_fasilitas'),
                    path('fasilitas/hapus/<int:id>', layanan.admin_fast_delete, name='admin_fast_delete'),
                ]
            )),

            # USERS
            path('user/', include(
                [
                    path('', user.admin_index, name='admin_user'),
                    path('create/', user.create, name='admin_user_create'),
                    path('password/edit/', user.edit_password, name='admin_user_password_edit'),
                    path('profile/edit/<str:username>', user.edit_profile, name='admin_user_edit'),
                    path('avatar/edit/<str:username>', user.edit_avatar, name='admin_user_avatar_edit'),
                    path('detail/', user.admin_detail, name='admin_user_detail'),
                    path('soft_delete/<str:username>', user.softDelete, name='admin_user_soft_delete'),
                    path('permanent_delete/<str:slug>', user.permanentDelete, name='admin_user_permanent_delete'),
                    path('restore/<str:username>', user.restore, name='admin_user_restore'),
                    path('edit_role/', user.edit_role, name='admin_user_edit_role'),
                    path('non_aktif/<int:id>', user.non_aktif, name='non_aktif'),
                    path('re_aktif/<int:id>', user.re_aktif, name='aktif'),
                ]
            )),

            # SETTING APLIKASI
            path('setting/', include(
                [
                    # path('', user.admin_index, name='admin_setting'),
                    path('category/', setting.admin_index_category, name='admin_setting_category'),
                    path('category/edit/', setting.admin_index_category_edit, name='admin_setting_category_edit'),
                    path('running_text/', setting.admin_index_running_text, name='admin_setting_running_text'),
                    path('running_text/edit/', setting.admin_index_running_text_edit, name='admin_setting_running_text_edit'),
                    path('link/', setting.admin_index_link, name='admin_setting_link'),
                    path('links/edit/', setting.admin_index_link_edit, name='admin_setting_link_edit'),
                    path('infografis/', setting.admin_index_infografis, name='admin_setting_infografis'),
                    path('download/', setting.download_file, name='download_file'),
                    path('cara_bayar/', setting.admin_index_carabayar, name='admin_setting_carabayar'),
                    path('carousel', setting.admin_index_carousel, name='admin_setting_carousel'),
                    path('create/carousel/', setting.create_carousel, name='admin_setting_create_carousel'),
                    path('edit/carousel/<int:id>', setting.edit_carousel, name='admin_setting_edit_carousel'),
                    path('running_link/', setting.admin_index_running_link, name='admin_setting_running_link'),
                    path('create/running_link/', setting.create_running_link, name='admin_setting_create_running_link'),
                    path('edit/running_link/<int:id>', setting.edit_running_link, name='admin_setting_edit_running_link'),
                    path('pages/', setting.admin_index_pages, name='admin_setting_pages'),
                    path('create/pages/', setting.create_pages, name='admin_setting_create_pages'),
                    path('edit/pages/<str:slug>', setting.edit_pages, name='admin_setting_edit_pages'),
                    path('detail/pages/<str:slug>', setting.detail_pages, name='admin_setting_detail_pages'),
                    path('soft_delete/<str:jenis>/<int:id>', setting.softDelete, name='admin_setting_soft_delete'),
                    path('permanent_delete/<str:jenis>/<int:id>', setting.permanentDelete, name='admin_setting_permanent_delete'),
                    path('restore/<str:jenis>/<int:id>', setting.restore, name='admin_setting_restore'),

                    path('me/<str:section>', setting.admin_index_aboutme, name='admin_setting_aboutme'),
                    path('create/me/<str:section>', setting.create_aboutme, name='admin_setting_create_aboutme'),
                    path('edit/me/<str:section>/<int:id>', setting.edit_aboutme, name='admin_setting_edit_aboutme'),
                    path('delete/<str:section>/<int:id>', setting.delete_aboutme, name='admin_setting_delete_aboutme'),

                    path('destinasi', setting.admin_index_destinasi, name='admin_setting_destinasi'),
                    path('create/destinasi/', setting.create_destinasi, name='admin_setting_create_destinasi'),
                    path('detail/destinasi/<int:id>', setting.detail_destinasi, name='admin_setting_detail_destinasi'),
                    path('edit/destinasi/<int:id>', setting.edit_destinasi, name='admin_setting_edit_destinasi'),
                ]
            )),
        ]
    ))
]